# Installation D'un dns et de son slave


Commande utilisée pour l'instalation du dns:

```
sudo yum install bind bind-utils
```

On édite le fichier **/etc/named.conf**

```
[root@apache ~]# cat /etc/named.conf
//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//
// See the BIND Administrator's Reference Manual (ARM) for details about the
// configuration located in /usr/share/doc/bind-{version}/Bv9ARM.html

options {
        listen-on port 53 { 127.0.0.1; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { localhost; };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        /* Path to ISC DLV key */
        bindkeys-file "/etc/named.iscdlv.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```


Puis on créer le fichier avec le nom de domaine voulu :
```
[root@dns named]# cat fwd.mydomain.com.db
@   IN  SOA     primary.mydomain.com. root.mydomain.com. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      primary.mydomain.com.

;Secondary Name Server
@      IN NS       secondary.mydomain.com.

;Apache Name Server
@      IN NS       apache.mydomain.com.

;IP address of Name Server
primary IN  A       192.168.125.133

;IP adress of Secondary Name Server
secondary IN A      192.168.125.132

;IP address of Apache Name Server
apache    IN A      192.168.125.128

;Mail exchanger
mydomain.com. IN  MX 10   mail.mydomain.com.

;A - Record HostName To IP Address
www     IN  A       192.168.125.128
mail    IN  A       192.168.125.150

;CNAME record
ftp     IN CNAME        www.mydomain.com.

```

Il faut ensuite penser a changer les adresses dns dans le fichier resolv.conf

On peut ensuite tester le bon fonctionnement avec dig:
```
[root@apache ~]# dig @192.168.125.133 www.mydomain.com

; <<>> DiG 9.9.4-RedHat-9.9.4-74.el7_6.2 <<>> @192.168.125.133 www.mydomain.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 19260
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 3, ADDITIONAL: 4

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;www.mydomain.com.              IN      A

;; ANSWER SECTION:
www.mydomain.com.       86400   IN      A       192.168.125.128

;; AUTHORITY SECTION:
mydomain.com.           86400   IN      NS      secondary.mydomain.com.
mydomain.com.           86400   IN      NS      primary.mydomain.com.
mydomain.com.           86400   IN      NS      apache.mydomain.com.

;; ADDITIONAL SECTION:
primary.mydomain.com.   86400   IN      A       192.168.125.133
secondary.mydomain.com. 86400   IN      A       192.168.125.132
apache.mydomain.com.    86400   IN      A       192.168.125.128

;; Query time: 0 msec
;; SERVER: 192.168.125.133#53(192.168.125.133)
;; WHEN: Thu Sep 12 09:40:53 EDT 2019
;; MSG SIZE  rcvd: 176
```
On peut égallement tester la redirection sur un navigateur sur l'hote, en changeant le dns sur la carte host only de virtualiseur que l'on utilise.