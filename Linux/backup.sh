#!/bin/bash
set -e
DATE=$(date +"%F_%Hh%Mm%Ss")

#on creer le dossier backup si il n existe pas

mkdir -p $HOME/backup

#on creer le fichier de log
touch /tmp/error.log

##on creer le fichier backupconf si il n'existe pas

if [ ! -e $HOME/.backupconf ]; then
   printf "MYSQL_USER= \nMYSQL_PASSWORD= \nDATABASE_NAME=\n" >> $HOME/.backupconf
echo veuillez compéter le ficher .backupconf ds home et relancer le script
exit 1
else
   echo fichier de conf ok
fi

# on clean le fichier erreur
rm /tmp/error.log
source $HOME/.backupconf

BACKUP_DIR="$HOME/backup"

MYSQL=/usr/bin/mysql
MYSQLDUMP=/usr/bin/mysqldump

mkdir -p $BACKUP_DIR/$DATE

#on se connecte a la mysql on save la db selectionnée  et on recupere le flux d erreur
mysqldump --user="$MYSQL_USER" --password="$MYSQL_PASSWORD" "$DATABASE_NAME" > "$BACKUP_DIR/$DATE/$DATABASE_NAME.sql" | 2>> /tmp/error.log & printf "$DATE\n" >> /tmp/error.log

#on affiche les erreurs si il y en a
cat /tmp/error.log

#on supprime les fichiers qui ont plus de 1 min ancienneté
find $HOME/backup/ -mmin +1 -delete