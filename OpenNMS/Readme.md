# Mise en place D' OpenNMS

J'ai choisis d'utiliser centOS car c est un système que je connais bien et avec lequel les configurations me sont aisée.

---

Commande utilisée :
- curl -L https://github.com/opennms-forge/opennms-install/archive/1.4.tar.gz | tar xz
- cd opennms-install-1.4
- yum install java-11-openjdk-devel -y
- bash bootstrap-yum.sh
- firewall-cmd --permanent --add-port=8980/tcp
- systemctl restart firewalld

---

On peut ensuite se connecter sur l'interface web:
http://mon-ip-local:8980/opennms/login.jsp

Les Identifiants par default sont admin, admin

OpenNMS est à present configuré.

# Fonctionnement, gestion, sauvegarde

## Fonctionnement :

Opennms est un outil de monitoring, on peut surveiller des routeurs, serveurs, switch, l'utilisation d'un lien ect...
On peut configurer toute sorte d'alerte ( avec différent niveau de gravité) pour etre tenu au courant. Il y a un système d'envoie d'alerte pas sms et email.

Commandes de bases :

Start:
```
[root@localhost ~]# systemctl start opennms
```
Stop
```
[root@localhost ~]# systemctl stop opennms
```
Status
```
[root@localhost ~]# systemctl status opennms
● opennms.service - OpenNMS server
   Loaded: loaded (/usr/lib/systemd/system/opennms.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2019-09-07 20:07:14 CEST; 23s ago
  Process: 3512 ExecStop=/opt/opennms/bin/opennms stop (code=exited, status=0/SUCCESS)
  Process: 4336 ExecStart=/opt/opennms/bin/opennms -Q start (code=exited, status=0/SUCCESS)
 Main PID: 5412 (opennms)
   CGroup: /system.slice/opennms.service
           ├─5412 /bin/bash /opt/opennms/bin/opennms -Q start
           └─5414 /usr/lib/jvm/java-11-openjdk-11.0.4.11-0.el7_6.x86_64/bin/java --add-modules=java.base,java.compile...

Sep 07 20:07:07 localhost.localdomain systemd[1]: Starting OpenNMS server...
Sep 07 20:07:14 localhost.localdomain opennms[4336]: Starting OpenNMS: (not waiting for startup) [  OK  ]
Sep 07 20:07:14 localhost.localdomain systemd[1]: Started OpenNMS server.
```

## Gestion :


Le monitoring de serveur ce fait avec un système de node, on rentre l'ip et la description du service que l'on veut monitorer.

---

![](./Photos/git1.PNG)


Après avoir fait:

```
systemectl stop httpd
```

On peut voir que le serveur est à present down, on voit également différentes informations comme son uptime, l'heure à laquel on l'a perdu, son type de service et un tableau récapitulatif des récents event.


![](./Photos/serveur_down.PNG)

---


## Sauvegarde :

On peut **sauvegarder** la db:
```
pg_dump --username=postgres --password opennms > opennms.sql
```

On peut **restaurer** la db:
```
pg_restore --username=postgres --password -Fc -d opennms /path/to/opennms.sql
```

On peut **supprimer** la db:
```
dropdb --username=postgres --password opennms
```
Lorsque l'on sauvegarde tt la configuration D'openNMS il faut faire attention aux versions de jre, iplike, postgres et d'openNMS. 
Il faut que les versions soient les même au début et à la fin de la restauration.

On peut **sauvegarder toute la configuration d'OpenNMS** :
(prend actuellement 700M et est assez longue)

```
tar -cvzf /home/opennms.tar.gz /opt/opennms/
```

Script de **Backup** :

Pour le cron:
```
touch /etc/cron.daily/opennms-backup
chmod +x /etc/cron.daily/opennms-backup
vi /etc/cron.daily/opennms-backup
```
Le script :
```
#!/bin/bash
 
# destination du backup
LOCALDIR=/mnt/backup
 
# verifi voir si le dossier existe
if ! [ -e $LOCALDIR ] ; then
    mkdir -p $LOCALDIR
fi
 
# timestamp
dsig=`date +%Y%m%d%H%M%S`
 
# Backup Postgres DB
pg_dump -U postgres -Fc -f $LOCALDIR/$dsig-database.pgsql.gz opennms
 
# Backup rrd
tar cvfz $LOCALDIR/$dsig-rrd.tar.gz /opt/opennms/share/rrd/
 
# Backup des config
tar cvfz $LOCALDIR/$dsig-config.tar.gz /opt/opennms/etc
 
# Supprime tt les backup de + de 30 jours
find $LOCALDIR -name '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]*.gz' -atime +30 -exec echo Deleting: {} \; -delete
```

## Shéma du projet OpenNMS:

Système utilisée pour ce tp:

-GNS3

-VMware Workstation

-Routeur 2960

-switch i86


A chaque lancement du projet sous gns3, je dois remonter tt les cartes de tt les vm car elles se déconnecte toutes automatiquement.
Chaque vm a sa carte atribué automatiquement, plus son host only .



![](./Photos/shéma.PNG)


L'adressage ip se fait avec un dhcp sur Vmware pour l'instant.


